package com.hw.db.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;

public class ThreadControllerTests {
    private Integer id;
    private String author;
    private String forum;
    private String message;
    private String slug;
    private String title;
    private Integer votes;
    private Thread thread;

    // private DataRetrievalFailureException Exception;

    private Post post;
    private ArrayList<Post> posts;
    private User user;
    private Vote vote;

    @BeforeEach
    void setup() {
        id = 228;
        author = "Nihon_V";
        forum = "Roc Zulip";
        message = "Roc Rocks";
        slug = "slug";
        title = "Title";
        votes = 1337;
        thread = new Thread(author, Timestamp.from(Instant.MIN), forum, message, slug, title, votes);
        thread.setId(id);

        post = new Post();
        post.setAuthor(author);

        posts = new ArrayList<>();
        posts.add(post);

        user = new User();
        user.setNickname(author);

        vote = new Vote(author, 228);
    }

    void setupThreadDaoMock(MockedStatic<ThreadDAO> threadDaoMock) {
        threadDaoMock.when(() -> ThreadDAO.getThreadById(id))
            .thenReturn(thread);
        threadDaoMock.when(() -> ThreadDAO.getThreadBySlug(anyString()))
            .thenThrow(new DataRetrievalFailureException("Unknown Slug"));
        threadDaoMock.when(() -> ThreadDAO.getThreadBySlug(slug))
            .thenReturn(thread);
        
    }
    void setupUserDaoMock(MockedStatic<UserDAO> userDaoMock) {
        userDaoMock.when(() -> UserDAO.Info(author))
            .thenReturn(user);
    }


    @Test
    @DisplayName("Correct post creation")
    void correctlyCreatesPost() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            setupThreadDaoMock(threadDaoMock);
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserDaoMock(userDAOMock);

                ThreadController threadController = new ThreadController();
                ResponseEntity response = threadController.createPost(slug, posts);

                Assertions.assertEquals(posts, response.getBody());
                Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
            }
        }
    }

    @Test
    @DisplayName("Get posts")
    void correctlyGetPosts() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            setupThreadDaoMock(threadDaoMock);
            threadDaoMock.when(() -> ThreadDAO.getPosts(any(), any(), any(), any(), any()))
                .thenReturn(posts);
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserDaoMock(userDAOMock);

                ThreadController threadController = new ThreadController();
                ResponseEntity response = threadController.Posts(slug, 10, 0, "", false);

                Assertions.assertEquals(posts, response.getBody());
                Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
            }
        }
    }

    @Test
    @DisplayName("Get posts by non-existent slug")
    void getNoPosts() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            setupThreadDaoMock(threadDaoMock);
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserDaoMock(userDAOMock);

                ThreadController threadController = new ThreadController();
                ResponseEntity response = threadController.Posts(slug + "gibberish", 10, 0, "", false);

                Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }

    @Test
    @DisplayName("Thread changes correctly")
    void correctlyChangesThread() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            setupThreadDaoMock(threadDaoMock);
            Thread new_thread = new Thread("a", Timestamp.from(Instant.MIN), "a", "a", "a", "a", 100);
            new_thread.setId(id+1);

            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserDaoMock(userDAOMock);

                ThreadController threadController = new ThreadController();
                ResponseEntity response = threadController.change(slug, thread);

                Assertions.assertEquals(thread, response.getBody());
                Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
            }
        }
    }

    @Test
    @DisplayName("Correctly retrieves thread")
    void correctlyGetsThreadInfo() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            setupThreadDaoMock(threadDaoMock);
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserDaoMock(userDAOMock);

                ThreadController threadController = new ThreadController();
                ResponseEntity response = threadController.info(slug);

                Assertions.assertEquals(thread, response.getBody());
                Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
            }
        }
    }

    @Test
    @DisplayName("Correctly create vote")
    void correctlyCreateVote() {
        try (MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class)) {
            setupThreadDaoMock(threadDaoMock);
            Integer new_votes = votes + vote.getVoice();
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                setupUserDaoMock(userDAOMock);

                ThreadController threadController = new ThreadController();
                ResponseEntity response = threadController.createVote(slug, vote);

                Assertions.assertEquals(thread, response.getBody());
                Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
                Assertions.assertEquals(thread.getVotes(), new_votes);
            }
        }
    }
}
